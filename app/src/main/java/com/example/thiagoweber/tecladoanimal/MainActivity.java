package com.example.thiagoweber.tecladoanimal;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clickcachorro(View v) {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.dogsound);
        mp.start();
    }

    public void clickgato(View v) {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.catsound);
        mp.start();
    }

    public void clickcavalo(View v) {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.horsesound);
        mp.start();
    }

    public void clickpassaro(View v) {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.birdsound);
        mp.start();
    }

    public void clickvaca(View v) {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.cowsound);
        mp.start();
    }

    public void clickovelha(View v) {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.sheepsound);
        mp.start();
    }

    public void clickpato (View v) {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.ducksound);
        mp.start();
    }

    public void clickgalinha (View v) {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.chickensound);
        mp.start();
    }
}